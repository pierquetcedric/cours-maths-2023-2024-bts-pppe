% !TeX document-id = {b3714b36-a33b-4d9c-bf4b-b04962fa552b}
% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\useproflyclib{piton}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{CascadiaMono}[Scale=MatchLowercase]
\usepackage{menukeys}
\let\tab\relax
\usepackage[boites]{cp-preambule}
\usepackage{worldflags}
\usepackage{sim-os-menus}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U3]},typedoc=ALGO,numdoc=02,titre={Fonctions et procédures}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%fancy
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}

\begin{document}

\begin{python}
def cube(x) :
	"""Retourne le cube du réel x"""
	res = x**3
	return res

def proc_cube() :
	x = 4
	print(f"Saisir un nombre x : {x}")
	res = x * x * x
	print(f"Le cube de {x} est {res}")

def affiche_somme(x,y) :
	print(f"{x}+{y} vaut {x+y}")

pi_approx = 3.14
def perimetre(R) :
	res = 2*R*pi_approx
	return res

def aire(R) :
	global resultat_aire
	resultat_aire = 3.14*R**2
	return resultat_aire

\end{python}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faPython,type=ALGO,swap}
\part{Fonctions et procédures}%<maincolor=TitreMarron,logo=\faPython,type=\DonneesTypeDoc,swap>

\section{Introduction}

\begin{cintro}[Compteur=false]
Nous avons déjà manipulé des fonctions et procédures. Par exemple en \calgpython, on a travaillé avec  \cpy{eval}, \cpy{import}, ou \cpy{print}.

\smallskip

Ce sont des \textit{vraies} fonctions, au sens algorithmique, telles qu'elles seront définies et étudiées dans ce chapitre.
\end{cintro}

\begin{crmq}[Pluriel,Compteur=false]
\begin{itemize}[leftmargin=*]
	\item Les \calg{fonctions} permettent notamment de décomposer un programme complexe en sous-programmes plus simples et d'améliorer ainsi la \textbf{lisibilité} d'un algorithme.
	
	Ces sous-programmes peuvent en outre être utilisés plusieurs fois au sein d'un même programme, évitant ainsi des répétitions de code.
	\item Un autre intérêt des \calg{fonctions} est qu'elles sont \textbf{réutilisables} dans d'autres programmes et par d'autres programmeurs.
\end{itemize}
\end{crmq}

\begin{cillustr}[Compteur=false]
L'image ci-dessous montre une petite partie du code \calgpython{} de la fonction \cpy{randint} qui sert à générer un entier aléatoire (en fait pseudo-aléatoire) :

\medskip

\hfill
\includegraphics[height=11cm]{chap02_algo_randint}
\hfill~

\medskip

Au lieu de devoir réécrire toutes ces instructions à chaque fois que l'on désire obtenir un entier pseudo-aléatoire par exemple entre 1 et 6, il suffit d'écrire \cpy{randint(1,6)}, c'est-à-dire
de faire \textbf{appel} à la fonction \cpy{randint} avec en \textbf{paramètres} 1 et 6.
\end{cillustr}

\begin{casavoir}[Compteur=false]
Tout comme les variables, les fonctions portent un nom. Les règles de nommage d'une fonction sont les mêmes que les règles de nommage d'une variable.
\end{casavoir}

\section{Définitions et structures}

\subsection{Définitions et premiers exemples}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Une \textcolor{red}{fonction} est un morceau de code pouvant recevoir en \textbf{entrée} une ou plusieurs valeurs à partir desquelles elle retourne une ou plusieurs valeurs en sortie.
	\item Les valeurs que la fonction reçoit en entrée s'appellent les \textbf{paramètres} ou les \textcolor{blue}{arguments} de la fonction.
	\item Les valeurs \textbf{retournées} par la fonction sont les \textcolor{teal}{résultats} de celle-ci.
\end{itemize}
\end{cdefi}

\begin{cillustr}[Compteur=false]
\hfill
\begin{tikzpicture}
	\node[very thick,draw=blue,text=blue!50!black,fill=blue!5,ellipse,minimum width=80pt,align=center,font=\sffamily] (a) {Paramètres ou \\ arguments} ;
	\node[right=80pt,very thick,draw=red,text=red!50!black,fill=red!5,rectangle,minimum width=80pt,minimum height=30pt,align=center,font=\sffamily] (b) at (a) {Fonction} ;
	\node[right=80pt,very thick,draw=teal,text=teal!5!black,fill=teal!10,ellipse,minimum width=80pt,align=center,font=\sffamily] (c) at (b) {Résultats retournés \\ par la fonction} ;
	\draw[very thick,->,>=latex] (a)--(b) ;
	\draw[very thick,->,>=latex] (b)--(c) ;
\end{tikzpicture}
\hfill~
\end{cillustr}

\begin{cexemple}
La \textcolor{red}{fonction} \calgpython{} ci-dessous prend en \textcolor{blue}{paramètre} un nombre à trois chiffres et \textcolor{teal}{retourne} les trois chiffres de ce nombre dans l'ordre.

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
def decompose(nombre):
	chiffre1 = nombre // 100
	reste = nombre % 100
	chiffre2 = reste // 10
	chiffre3 = reste % 10
	return chiffre1, chiffre2, chiffre3
\end{CodePiton}

La \textcolor{red}{fonction} ci-dessus, nommée \cpy{decompose}, a les caractéristiques suivantes :

\begin{itemize}
	\item elle a un unique \textcolor{blue}{paramètre} nommé \cpy{nombre} ;
	\item elle \textcolor{teal}{retourne} trois résultats qui sont nommés \cpy{chiffre1}, \cpy{chiffre2} et \cpy{chiffre3}.
\end{itemize}
\end{cexemple}

\subsection{Création}

\begin{calgo}
Pour définir une \calg{fonction}, on utilise la syntaxe suivante :

\smallskip

$\bullet$ En \calg{pseudo-code} :

\begin{PseudoCodeAlt}*[Couleur,Largeur=14cm]{center}
Fonction nom_fonction(parametre_1, parametre_2, ...) :
Variables locales : ...
Debut Fonction
	instructions
	Retourner : valeur_1, valeur_2 ,...
Fin Fonction
\end{PseudoCodeAlt}

\smallskip

$\bullet$ En \calgpython{} :

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane,Lignes=false]{}
def nom_fonction(parametre_1, parametre_2 , ...) :
	instructions
	return valeur_1, valeur_2 ,...
\end{CodePiton}
\end{calgo}

\begin{cexemple}[Pluriel]
\begin{minipage}[t]{0.5\linewidth}
En \calg{pseudo-code} :

\begin{PseudoCodeAlt}[Largeur=7cm,Couleur]{center}
Fonction cube(x) :
Variables locales : x, res
Debut Fonction :
	res = x * x * x
	Retourner res
Fin Fonction
\end{PseudoCodeAlt}
\end{minipage}
%
\begin{minipage}[t]{0.5\linewidth}
En \calgpython{} :

\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}
def cube(x) :
	res = x * x * x
	return res
\end{CodePiton}
\end{minipage}

\smallskip

\begin{minipage}{0.5\linewidth}
Utilisation (suite de l'algorithme) :

\begin{PseudoCodeAlt}[Largeur=7cm,Couleur,PremLigne=7]{center}
a = cube(3)
Afficher : a
Afficher : cube(4)
Afficher : cube(5)
\end{PseudoCodeAlt}
\end{minipage}
%
\begin{minipage}{0.5\linewidth}
Utilisation (suite de l'algorithme) :

\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane]{}<start=4>
a = cube(3)
print(a)
print(cube(4))
print(cube(5))
\end{CodePiton}
\end{minipage}

\medskip

La console permet de vérifier le bon fonctionnement de cette fonction (les \cpy{print} ne sont pas obligatoires si les lignes d'affichage sont saisies dans la console directement !) :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
a = cube(3)
a
cube(4)
cube(5)
\end{ConsolePiton}
\end{cexemple}

\begin{crmq}[Pluriel,Compteur=false]
\begin{itemize}[leftmargin=*]
	\item Les parenthèses après le nom de la fonction sont obligatoires. Si une fonction n'a pas de
	paramètre, on ne met rien entre les parenthèses.
	\item Le symbole \cpy{:} après les parenthèse est également obligatoire.
	\item L'instruction \calg{Retourner} ou \cpy{return} précède la liste des valeurs retournées par la fonction.
	
	\textbf{Attention, les instructions -- algorithmiquement parlant -- après le \cpy{return} ne sont pas effectuées.}
\end{itemize}
\end{crmq}

\begin{cdefi}
En \calgpython, les instructions qui sont dans la définition de la fonction (ou dans n'importe quelle autre structure de programmation) commencent par un espace horizontal appelé \textbf{indentation}.

L'indentation s'obtient grâce à la touche tabulation, via les touches \menu{\tabmac} ou \menu{\tabwin} (on peut également l'obtenir en appuyant quatre fois sur la touche \menu{Espace}. Elle est \textbf{obligatoire} en Python.
\end{cdefi}

\begin{crmq}[Pluriel,Compteur=false]
L'\textbf{indentation} fait partie du code, elle sert à indiquer le début et la fin d'une \textit{structure} : les instructions indentées sont dans la structure, celles qui ne sont pas indentées sont hors de la structure.

Les règles d'indentation sont donc à respecter scrupuleusement sous peine d'erreur(s) !!
\end{crmq}

\pagebreak

\subsection{Les procédures}

\begin{cdefi}
Il existe des structures en tout point similaires à des fonctions, mais ne retournant aucun résultat ou comprenant des \og Entrées/Sorties \fg{} (\cpy{input}/\cpy{print}).

Dans ce cas, on parle de procédures, qui se construisent de la même façon que les fonctions en \calgpython{} (et en \calg{pseudo-code}), mais elles sont autorisées à contenir d'autres que éléments que \cpy{return}.
\end{cdefi}

\begin{crmq}[Compteur=false]
Une procédure pourra être utile dans le cas où une interaction avec l'utilisateur sera \textit{nécessaire}, avec notamment des \textit{questions} posées, et des \textit{conclusions} affichées.
\end{crmq}

\begin{calgo}
La fonction \cpy{cube} précédente peut-être modifiée en procédure, mais dans ce cas on perd une information \textit{fondamentale}, le \calg{type} du résultat !

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
def proc_cube() :
	x = eval(input("Saisir un nombre x : "))
	res = x * x * x
	print(f"Le cube de {x} est {res}")
\end{CodePiton}

\medskip

La console permet de vérifier le (bon) fonctionnement de cette procédure, et de constater une différence fondamentale entre une fonction et une procédure :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
cube(4)
type(cube(4))

cube(3.75)
type(cube(3.75))

proc_cube()
type(proc_cube())
\end{ConsolePiton}
\end{calgo}

\section{Appel d'une fonction ou d'une procédure}

\subsection{Introduction}

\begin{crmq}[Compteur=false]
Définir une fonction est un peu comme écrire une recette de cuisine : on fait la liste des ingrédients (paramètre d'entrée), puis on note les instructions à effectuer avec ces ingrédients pour arriver à un résultat final.

Mais une fois qu'on a fini d'écrire la recette de cuisine, on n'a toujours rien cuisiné : il faut que quelqu'un réunisse les ingrédients et effectue les instructions de la recette avec ceux-ci pour obtenir le plat.
\end{crmq}

\pagebreak

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Définir une fonction veut dire écrire les instructions constituant cette fonction (c'est-à-dire, en \calgpython, le \cpy{def} et tout ce qui est \textit{indenté} ensuite).
	\item Pour exécuter les instructions constituant une fonction, il faut avoir recours à une instruction qui porte le nom d'appel à la fonction.
\end{itemize}
\end{cdefi}

\begin{crmq}[Compteur=false]
\textbf{Attention :} sans appel, aucune instruction n'est exécutée !!
\end{crmq}

\subsection{Appel d'une fonction}

\begin{calgo}
Il existe deux cas de figure pour l'appel d'une fonction :

\begin{itemize}[leftmargin=*]
	\item \textsc{Premier cas} : on veut utiliser le résultat de la fonction dans des instructions ultérieures. Dans ce cas, il est nécessaire de \calg{stocker} le résultat de la fonction dans une
	\calg{variable}.
	
	\begin{minipage}[t]{0.5\linewidth}
		En \calg{pseudo-code} :
		
\begin{PseudoCodeAlt}*[Couleur,Largeur=7cm]{center}
var = nom_fct(params)
\end{PseudoCodeAlt}
	\end{minipage}
	%
	\begin{minipage}[t]{0.5\linewidth}
		En \calgpython{} :
		
		\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane,Lignes=false,Gobble=tabs]{}
			var = nom_fct(params)
		\end{CodePiton}
	\end{minipage}
	\item \textsc{Deuxième cas} : on veut juste afficher le résultat de la fonction (pour tester celle-ci par exemple).
	
	\begin{minipage}[t]{0.5\linewidth}
		En \calg{pseudo-code} :
		
\begin{PseudoCodeAlt}*[Couleur,Largeur=7cm]{center}
Afficher : nom_fct(params)
\end{PseudoCodeAlt}
	\end{minipage}
	%
	\begin{minipage}[t]{0.5\linewidth}
		En \calgpython{} :
		
		\begin{CodePiton}[Style=Classique,Largeur=7cm,Alignement=center,Filigrane,Lignes=false,Gobble=tabs]{}
			print(nom_fct(params))
		\end{CodePiton}
	\end{minipage}
\end{itemize}
\end{calgo}

\begin{cexemple}[Compteur=false]
On peut vérifier dans la console \calgpython{} :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
# appel de la fonction min (premier cas)
a = min(5, 42)
print(a)

# appel de la fonction min (deuxième cas)
print(min(5, 42))
min(5, 42)
\end{ConsolePiton}
\end{cexemple}

\subsection{Appel d'une procédure}

\begin{calgo}
Pour appeler une procédure, on utilise la syntaxe suivante :

\begin{minipage}[t]{0.5\linewidth}
	En \calg{pseudo-code} :
	
\begin{PseudoCodeAlt}*[Largeur=7cm]{center}
nom_proc(params)
\end{PseudoCodeAlt}
\end{minipage}
%
\begin{minipage}[t]{0.5\linewidth}
	En \calgpython{} :
	
	\begin{CodePiton}[Lignes=false,Style=Classique,Largeur=7cm,Alignement=center,Filigrane,Gobble=tabs]{}
		nom_proc(params)
	\end{CodePiton}
\end{minipage}
\end{calgo}

\pagebreak

\begin{cexemple}
On va définir et appeler une procédure \cpy{affiche\_somme} en \calgpython{} :

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
# Définition :
def affiche_somme(x,y) :
	print(f"{x}+{y} vaut {x+y}")
\end{CodePiton}

Dans ce cas, il n'y a pas besoin de \cpy{print} dans un script ou dans la console :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
a = affiche_somme(45, 27)
print(a)

affiche_somme (45, 27)
print(affiche_somme (45, 27))
\end{ConsolePiton}

En cas d'affichage non voulu de \cpy{None}, vérifiez que vous n'avez pas fait appel à une procédure avec la méthode d'appel d'une fonction !!
\end{cexemple}

\subsection{Aide d'une fonction ou d'une procédure}

\begin{cidee}[Compteur=false]
En \calgpython{}, on commence en général la fonction par une ligne de documentation écrite entre triples guillemets (les \textsf{docstrings}). Cette ligne n'est pas obligatoire, mais documenter ses fonctions est une bonne habitude à prendre.

\smallskip

Il suffit dans ce cas d'utiliser la commande \cpy{help} pour afficher l'aide située dans les \textsf{docstrings}.
\end{cidee}

\begin{calgo}
On peut reprendre la fonction \cpy{cube} et luis rajouter une aide :

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
def cube(x) :
	"""Retourne le cube du réel x"""
	res = x**3
	return res
\end{CodePiton}

Et dans ce cas la console permettra d'afficher l'aide :

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
help(cube)
\end{ConsolePiton}
\end{calgo}

\pagebreak

\section{Variables locales, variables globales}

\subsection{Variables locales}

\begin{cdefi}
Lorsque des variables sont définies à l'intérieur d'une procédure ou d'une fonction, elles ne sont accessibles que dans cette dernière. On dit que ce sont des \textbf{variables locales}.

Les paramètres d'une fonction sont également des variables locales.
\end{cdefi}

\begin{calgo}
Par exemple, la variable \cpy{res} présente dans la fonction \cpy{cube} est locale, elle n'existe \textit{plus} une fois la fonction appelée.

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
cube(11)
res
\end{ConsolePiton}
\end{calgo}

\subsection{Variables globales}

\begin{cdefi}
Les variables définies à l'extérieur d'une procédure ou d'une fonction sont appelées \textbf{variables globales}.

Elles sont accessibles à l'intérieur d'une fonction si une variable locale de même nom n'existe pas.
\end{cdefi}

\begin{calgo}
Par exemple, si on définit une variable \cpy{pi\_approx}, on peut définir une fonction permettant de retourner le périmètre d'un cercle de rayon donné.

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
pi_approx = 3.14
def perimetre(R) :
	res = 2*R*pi_approx
	return res
\end{CodePiton}

On peut vérifier que la variable \cpy{pi\_approx} est correctement utilisée dans la fonction.

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
perimetre(4)
\end{ConsolePiton}
\end{calgo}

\begin{crmq}[Compteur=false]
Il est également possible de spécifier qu'une variable est globale, directement dans le corps d'une fonction, grâce à la commande \cpy{global}.

\begin{CodePiton}[Style=Classique,Largeur=14cm,Alignement=center,Filigrane]{}
def aire(R) :
	global resultat_aire
	resultat_aire = 3.14*R**2
	return resultat_aire
\end{CodePiton}

\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
aire(10)
resultat_aire

aire(25)
resultat_aire
\end{ConsolePiton}
\end{crmq}

\section{Fonctions prédéfinies}

\subsection{Définitions}

\begin{cdefi}[Pluriel]
En \calgpython, il existe un très grand nombre de fonctions prédéfinies. Il n'est pas pratique de charger toutes ces fonctions en mémoire. Une grosse partie d'entre elles sont donc stockées dans des fichiers appelés \textbf{modules} qu'il faut importer en mémoire avant utilisation.
\end{cdefi}

\subsection{Modules}

\begin{calgo}
Pour charger un module en mémoire, on utilise l'instruction \cpy{import}.
\end{calgo}

\begin{cexemple}[Pluriel]
Pour utiliser la fonction \cpy{randint} du module \cpy{random}, on peut utiliser l'instruction \cpy{import} de plusieurs façons différentes :

\begin{itemize}[leftmargin=*]
	\item \textsc{Méthode 1 :} en important uniquement la fonction voulue depuis (\cpy{from} en anglais) le module dans l'espace de noms principal :
	
	\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
		from random import randint
		# utilisation
		randint(0, 10) # ou print(randint(0, 10)) dans un script
	\end{ConsolePiton}
	\item \textsc{Méthode 2 :} en important toutes les fonctions du module dans l'espace de noms principal :
	
	\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
		from random import *
		# utilisation
		randint(0, 10) # ou print(randint(0, 10)) dans un script
	\end{ConsolePiton}
\end{itemize}
\end{cexemple}

\begin{crmq}[Compteur=false]
On peut également, au cas où plusieurs fonctions porteraient le même nom, charger un module avec un \textit{préfixe} de chargement :

\begin{itemize}[leftmargin=*]
	\item \textsc{Méthode 3 :} en important toutes les fonctions du module dans un sous-espace de l'espace de noms principal ; et dans ce cas pour utiliser les fonctions du module, il faut alors spécifier l'espace de noms dans lequel elles ont été chargées ; et par défaut, ce nom est le nom du module :
	
	\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
		import random
		# utilisation
		random.randint(0, 10) # ou print(random.randint(0, 10)) dans un script
	\end{ConsolePiton}
	\item \textsc{Méthode 4 :} en important toutes les fonctions du module dans un sous-espace de l'espace
	de noms principal, pour lequel on choisit l'espace de noms :
	
	\begin{ConsolePiton}<Largeur=14cm,Alignement=center>{}
		import random as bob
		# utilisation
		bob.randint(0, 10) # ou print(bob.randint(0, 10)) dans un script
	\end{ConsolePiton}
\end{itemize}
\end{crmq}

\begin{calgo}[Compteur=false]
Pour obtenir la liste des fonctions contenues dans un module, on utilise la commande help
suivi du nom du module entre guillemets.

Par exemple, en \textit{ligne de commandes} (ou dans la console), on aura la sortie (tout n'est pas affiché !) suivante :

\begin{TerminalWin}[Largeur=14cm,Align=center,Titre={Terminal}]{}
C:\Users\user>python
Python 3.11.5
Type "help", "copyright", "credits" or "license" for more information.
>>> help("random")
Help on module random:

NAME
    random - Random variable generators.

MODULE REFERENCE
    https://docs.python.org/3.11/library/random.html
    The following documentation is automatically generated from the Python source files. It may be incomplete, incorrect or include features that are considered implementation detail and may vary between Python implementations. When in doubt, consult the module reference at the location listed above.

DESCRIPTION
    bytes
    -----
        uniform bytes (values between 0 and 255)

    integers

-- Suite  --
\end{TerminalWin}
\end{calgo}

\begin{casavoir}[Compteur=false]
Le module \cpy{random} et sa fonction \cpy{randint} seront régulièrement utilisés et sont à connaître pour l'examen !
\end{casavoir}

\end{document}