% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: yes, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{piton,ecritures}
\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{CascadiaMono}[Scale=MatchLowercase]
\usepackage[boites]{cp-preambule}
\usepackage{systeme}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=BTS SIO1,matiere={[U3]},typedoc=CHAP,numdoc=05,titre={Matrices}]
%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faProjectDiagram,type=CHAP,swap}
\part{Matrices}%<maincolor=TitreMarron,logo=\faProjectDiagram,type=\DonneesTypeDoc,swap>%

\section{Notion de matrice}

\subsection{Introduction}

\begin{cintro}[Compteur=false]
Les matrices sont des \textbf{tableaux rectangulaires} (ou carrés) constitués de nombres et sont des outils qu'on utilise fréquemment lorsqu'on veut représenter une situation qui peut comporter plusieurs « entrées » et plusieurs « sorties » (\textit{input-output}).
\end{cintro}

\subsection{Définitions}

\begin{cdefi}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Si $n$ et $p$ sont deux entiers naturels non nuls, on appelle \textbf{matrice réelle} de \textbf{dimensions} $n \times p$ tout tableau M de nombres réels possédant $n$ lignes et $p$ colonnes.
	\item Dans une matrice M, il y a donc $n \times p$ nombres, qu'on appelle les \textbf{coefficients} (éléments ou termes) de M.
	\item Lorsque $p = n$ (autant de colonnes que de lignes), on parle de matrice carrée.
	
	Pour une matrice M carrée d'ordre $n$ les coefficients qui sont « visuellement » situés sur la diagonale descendant d'en haut à gauche jusqu'en bas à droite sont appelés coefficients diagonaux de M.
	\item La dimension de la matrice M est désignée par $n \times p$.
	\item Le terme de M situé à la $i$ème ligne ($1\pp i \pp n$) et à la $j$ème colonne ($1\pp j \pp p$) est noté $a_{ij}$.
\end{itemize}
\end{cdefi}

\begin{cexemple}
Soit A la matrice $\begin{pmatrix}10&12&4&2\\1&7&8&9\end{pmatrix}$. La matrice A de dimension $2 \times 4$. Et : \[a_{21}=1 \quad a_{22}=7 \quad a_{14}=2.\]
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut manipuler une matrice grâce aux \og tableaux à deux dimensions \fg, en faisant attention aux indices qui commencent à 0 !

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
A = [[10, 12, 4, 2], [1, 7, 8, 9]]
A[1][0]
A[1][1]
A[0][3]
\end{ConsolePiton}
\end{cpython}

\begin{cprop}[ComplementTitre={ - Matrices particulières}]
Si $n=1$ :  la matrice M est une \textbf{matrice ligne} $M = \begin{pmatrix} a_1 & a_2 & a_3 & \ldots & \ldots & a_p \end{pmatrix}$.

\medskip

Si $p=1$ : la matrice M est une \textbf{matrice colonne} $M = \begin{pmatrix}a_1 \\ a_2 \\ \ldots \\ \ldots \\ a_n \end{pmatrix}$.
\end{cprop}

\subsection{Matrice nulle, matrice identité}

\begin{cdefi}[Pluriel]
Une matrice nulle est une matrice ne contenant que des \og 0 \fg. On peut la noter $O$.

\medskip

La matrice carrée $I_n$ d’ordre $n$ est une \textbf{matrice identité} (ou \textbf{matrice unité}) si les coefficients diagonaux sont tous égaux à 1 et les autres coefficients sont tous égaux à 0.
%
\[I_3 = \begin{pmatrix}1&0&0\\0&1&0\\0&0&1 \end{pmatrix} \text{ est la matrice identité d'ordre 3.}\]
\end{cdefi}

\section{Calcul matriciel}

\subsection{Égalité de deux matrices}

\begin{cprop}
Deux matrices sont égales si elles ont :
\begin{itemize}[leftmargin=*]
	\item les mêmes dimensions ;
	\item les mêmes coefficients.
\end{itemize}
\end{cprop}

\begin{cexemple}
Par exemple, les matrices $A=\MatDeux{1}{2}{0}{-1}$ et $B=\MatDeux{2}{1}{0}{-1}$ ne sont pas égales (coefficients de la première ligne\ldots).
\end{cexemple}

\subsection{Addition de deux matrices}

\begin{cdefi}
Soient $A=\left(a_{ij} \right)$ et $B=\left(b_{ij} \right)$ deux matrices de même dimension $n \times p$.

\medskip

La matrice somme des matrices A et B est la matrice notée  $A+B= \left(c_{ij} \right)$ à $n$ lignes et $p$  colonnes telle que : \[c_{ij}=a_{ij}+b_{ij}.\]
\end{cdefi}

\begin{crmq}
Ajouter deux matrices de même dimension $n \times p$ revient à faire $n \times p$ additions de réels. Les propriétés de l'addition des matrices sont donc les mêmes que les propriétés bien connues de l'addition des réels. 
\end{crmq}

\begin{cprop}[Pluriel]
Quelles que soient les matrices A, B, C à $n$ lignes et à $p$ colonnes :

\begin{itemize}[leftmargin=*]
	\item $A+B=B+A$. \hfill{}(commutativité)
	\item $(A+B)+C=A+(B+C)=A+B+C$. \hfill{}(associativité)
	\item $A+O=O+A=A$. \hfill{}(la matrice nulle de dimension $n \times p$ est élément neutre pour l’addition)
	\item Il existe une matrice notée $-A$ telle que $A+(-A)=(-A)+A=O$ ; la matrice $-A$ est la matrice opposée à A.
	
	La matrice $-A$ est la matrice dont les termes sont les opposés des termes de la matrice A.
	\item L’existence, pour toute matrice A, d’une matrice opposée, permet de définir une soustraction de matrices (de même dimension) : $A-B=A+(-B)$.
\end{itemize}
\end{cprop}

\begin{cexo}
On donne $A = \begin{pmatrix} 3&-2&1\\-1&0&4\end{pmatrix}$ et $B=\begin{pmatrix} 0&1&-1\\-3&-8&4\end{pmatrix}$. A et B sont de même dimensions : \[A+B =  \begin{pmatrix} 3&-1&0 \\ -4&-8&8 \end{pmatrix} \text{ et } A-B = \begin{pmatrix} 3&-3&2 \\ 2&8&0 \end{pmatrix}.\]
\end{cexo}

\begin{cpython}
En \calgpython{}, on ne peut pas additionner (et a fortiori soustraire) deux tableaux (le \cpy{+} est ici une concaténation !), il faut donc passer par un \cpy{module} permettant de faire du calcul matriciel, le module \cpy{numpy}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
A, B = [[3, -2, 1], [-1, 0, 4]], [[0, 1, -1], [-3 ,-8, 4]]
A+B
A-B
import numpy as np             #on importe numpy et on préfixe ses commandes avec np
A=np.asmatrix('[3 -2 1 ; -1 0 4]')
B=np.asmatrix('[0 1 -1 ; -3 -8 4]')
A+B
A-B
\end{ConsolePiton}
\end{cpython}

\subsection{Multiplication par un réel}

\begin{cdefi}
Si A est une matrice de dimensions $n \times p$ et si $k$ est un nombre réel, on définit la matrice $kA$ comme la matrice obtenue en multipliant chaque coefficient de A par le réel $k$. $$kA=\left( k \times a_{ij} \right).$$
\end{cdefi}

\begin{cexo}
Si $ A = \begin{pmatrix} 1&0\\2&-1 \end{pmatrix}$ alors $3A= \begin{pmatrix} 3&0\\6&-3 \end{pmatrix}$ et $-5A=\begin{pmatrix} -5&0\\-10&5 \end{pmatrix}$.
\end{cexo}

\begin{cpython}
En \calgpython{}, on peut utiliser le calcul matriciel du module \cpy{numpy}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
import numpy as np             #on importe numpy et on préfixe ses commandes avec np
A=np.asmatrix('[1 0 ; 2 -1]')
3*A
-5*A
\end{ConsolePiton}
\end{cpython}

\begin{cprop}[Pluriel]
Quelles que soient les matrices A et B et les réels $k$ et $k'$ :

\begin{itemize}[leftmargin=*]
	\item $k(A+B)=kA+kB$
	\item $(k+k')A=kA+k'A$
	\item $k(k'A)=(kk')A$
\end{itemize}
\end{cprop}

\section{Produit de deux matrices}

\subsection{Introduction}

\begin{cmethode}
Pour multiplier deux matrices, on dit qu’on effectue le produit « \textbf{ligne  par colonne} ».

Le produit de matrices $A \times B$ n'est été possible que parce que le nombre de \textbf{colonnes} de la matrice A est égal au nombre de \textbf{lignes} de  la matrice B (\og on pose en triangle \fg).
\end{cmethode}

\subsection{Définition, propriétés}

\begin{cdefi}
Soient $A=\left(a_{ij} \right)$ une matrice $n \times p$ et $B=\left(b_{ij} \right)$ une matrice $p \times r$.

Le produit de la matrice A par la matrice B est la matrice notée $AB=\left(c_{ij} \right)$ de dimension $n \times r$ et définie par : \[c_{ik}=a_{i1} b_{1k}+a_{i2} b_{2k}+ \dots +a_{ip} b_{pk}.\]
\end{cdefi}

\begin{cexemple}
Si $A=\begin{pmatrix}1&2\\0&3\end{pmatrix}$ et $A=\begin{pmatrix}-2&-3\\2&5\end{pmatrix}$, alors $A \times B = \begin{pmatrix}\mathcolor{red}{2}&\mathcolor{blue}{7}\\ \mathcolor{CouleurVertForet}{6}&\mathcolor{orange}{15}\end{pmatrix}$ car {\scriptsize $\begin{dcases} \mathcolor{red}{1 \times(-2) + 2 \times 2=2}\\ \mathcolor{blue}{1 \times(-3) + 2 \times 5=7}\\ \mathcolor{CouleurVertForet}{0 \times (-2) + 3 \times 2=6}\\ \mathcolor{orange}{0 \times (-3) + 3 \times 5 = 15} \end{dcases}$} ; et $B \times A = \begin{pmatrix}-2&-13\\2&19\end{pmatrix}$.
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut utiliser le calcul matriciel du module \cpy{numpy}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
import numpy as np             #on importe numpy et on préfixe ses commandes avec np
A=np.asmatrix('[1 2 ; 0 3]')
B=np.asmatrix('[-2 -3 ; 2 5]')
A*B
B*A
\end{ConsolePiton}
\end{cpython}

\begin{cprop}[Pluriel]
Quelles que soient les matrices $A$, $B$ et $C$ et le réel $k$ :

\begin{itemize}[leftmargin=*]
	\item $A \times (B \times C)=(A\times B)\times C$ \hfill{}(associativité)
	\item $A\times(B+C)=A\times B+A\times C$ et $(B+C)\times A=B\times A+C \times A$
	\item $A\times (kB)=(kA)\times B=k(A\times B)$
	\item Dans le cas des matrices carrées d’ordre $n$, la matrice identité $I_n$ est telle que $A \times I_n=I_n \times A=A$
\end{itemize}
\end{cprop}

\begin{crmq}[Pluriel]
Le produit de deux matrices :
\begin{itemize}[leftmargin=*]
	\item n’est pas toujours possible (les dimensions doivent être compatibles)
	\item n’est pas commutatif
	\item peut être égal à une matrice nulle sans pour autant que l’une des matrices ne soit une matrice nulle. 
\end{itemize}
\textbf{IL N'EXISTE PAS DE DIVISION ENTRE MATRICES}
\end{crmq}

\section{Matrice inverse}

\subsection{Définition}

\begin{cdefi}
Si M est une matrice carrée d’ordre $n$, on dit que M est \textbf{inversible} s’il existe une matrice carrée d’ordre $n$ notée $M^{-1}$ vérifiant : $MM^{-1}=M^{-1} M=I_n$ ($I_n$ désigne la matrice identité d’ordre $n$)

\medskip

Cette matrice $M^{-1}$, si elle existe, est unique et est appelée matrice inverse de la matrice M.
\end{cdefi}

\begin{crmq}
Il existe des matrices non inversibles.
\end{crmq}

\begin{cexemple}[Pluriel]
La calculatrice permet de déterminer l'inverse (quand elle existe) d'une matrice carrée :
\begin{itemize}
	\item si $A=\begin{pmatrix}1&3\\-2&2\end{pmatrix}$, alors $A$ est inversible et $A^{-1}=\begin{pmatrix}\nicefrac{1}{4}&\nicefrac{3}{8}\\ \nicefrac{1}{4}&-\nicefrac{1}{8}\end{pmatrix}$ ;
	\item si $B=\begin{pmatrix}1&1&1\\0&-3&2\\-2&-2&-2\end{pmatrix}$, alors $B$ n'est pas inversible (on parle de matrice singulière).
\end{itemize}
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut utiliser le calcul matriciel du module \cpy{numpy}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
import numpy as np             #on importe numpy et on préfixe ses commandes avec np
A, B = np.asmatrix('[1 3 ; -2 2]'), np.asmatrix('[1 1 1 ; 0 -3 2 ; -2 -2 -2]')
A**(-1)
#et B**(-1) donnerait une erreur du type numpy.linalg.LinAlgError: Singular matrix
\end{ConsolePiton}
\end{cpython}

\subsection{Résolution de systèmes linéaires}

\begin{cexemple}
\[(S) \: : \: \systeme{x-2y+2z=1,2x+12y+z=5,3x-12y+8z=1}\] est un système \textbf{linéaire} de \textbf{trois} équations à \textbf{trois} inconnues $(x;y;z)$.

\medskip

Le triplet $(3;0;-1)$ est une \textbf{solution} du système : en effet, lorsque l’on remplace $x$ par 3, $y$ par 0 et $z$ par $-1$, les trois égalités sont \textbf{vraies}.
\end{cexemple}

\begin{cdefi}
Soit $(S)$ un système linéaire de trois équations à trois inconnues. Il est de la forme : \[\systeme[xyz]{a x+b y+c z=m,d x +e y+f z=n,g x+h y+k z=p}\] Où $a$, $b$, $c$, $d$, $e$, $f$, $g$, $h$, $k$, $m$, $n$, $p$ sont des nombres réels et $x$, $y$, $z$ sont les inconnues.

\medskip

Résoudre ce système consiste à trouver tous les triplets de réels $(x;y;z)$ qui vérifient les trois équations.
\end{cdefi}

\begin{crmq}
D’une manière générale, résoudre dans $\R$ un système linéaire de $n$ équations à $n$ inconnues consiste à déterminer l’ensemble des $n$-uplets de réels qui vérifient en même temps les $n$ équations.
\end{crmq}

\begin{cmethode}
Le passage par les matrices permet :
\begin{itemize}[leftmargin=*]
	\item transformer matriciellement le système sous le forme $AX=B$ ;
	\item utiliser (si elle existe) l'inverse de A pour en déduire $X=A^{-1}B$.
\end{itemize}
\end{cmethode}

\begin{cdemo}[Compteur=false,ComplementTitre={ souvent demandée dans les exos Type BTS}]
Si A est inversible, on a \[AX=Y \ssi A^{-1} \times A \times X = A^{-1} \times Y \ssi I \times X = A^{-1}\times Y \ssi X=A^{-1}B.\]
\end{cdemo}

\begin{cexemple}
On souhaite résoudre le système donné dans l'exemple introductif.

En utilisant le produit matriciel, on remarque que ce système peut s'écrire sous la forme $AX=Y$

où $A$, $X$, $Y$ sont les matrices : $A=\begin{pmatrix}1&-2&2\\2&12&1\\3&-12&8\end{pmatrix} \text{ ; } X=\begin{pmatrix}x\\y\\z\end{pmatrix} \text{ et } Y=\begin{pmatrix}1\\5\\1\end{pmatrix}$.

La calculatrice donne $A^{-1}=\begin{pmatrix}54/7&-4/7&-13/7\\-13/14&1/7&3/14\\-30/7&3/7&8/7\end{pmatrix}$.

On obtient $X=A^{-1}Y$, soit $\begin{pmatrix}x\\y\\z\end{pmatrix}=\begin{pmatrix}54/7&-4/7&-13/7\\-13/14&1/7&3/14\\-30/7&3/7&8/7\end{pmatrix}\begin{pmatrix}1\\5\\1\end{pmatrix}=\begin{pmatrix}3\\0\\-1\end{pmatrix}$. Et ainsi $\mathscr{S}=\left\lbrace \big(\strut3;0;-1\big) \right\rbrace$.
\end{cexemple}

\begin{cpython}
En \calgpython{}, on peut utiliser le calcul matriciel du module \cpy{numpy}.

\smallskip

\begin{ConsolePiton}<Largeur=16cm,Alignement=center>{}
import numpy as np             #on importe numpy et on préfixe ses commandes avec np
A, B = np.asmatrix('[1 -2 2 ; 2 12 1 ; 3 -12 8]'), np.asmatrix('[1 ; 5 ; 1]')
A**(-1)
A**(-1)*B #attention à la précision des calculs numériques...
\end{ConsolePiton}
\end{cpython}

\end{document}