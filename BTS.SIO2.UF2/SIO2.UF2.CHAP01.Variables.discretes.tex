% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{Fira Mono}[Scale=MatchLowercase]
\usepackage[boites]{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[UF2]},typedoc={CHAP},numdoc={1},titre={Variables discrètes}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers
\newfontfamily\polancienne{QTAntiquePost}

\begin{document}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faDice,type=CHAP,swap}
\part{Variables aléatoires discrètes}%<maincolor=TitreMarron,logo=\faDice,type=\DonneesTypeDoc,swap>

\section{Généralités}

\subsection{Exemple}

\begin{cexemple}[Compteur=false]
On lance trois pièces de monnaie et on note pour chacune d'elles le côté qui apparaît.

L'\textbf{univers} associé à cette expérience aléatoire est $\Omega=\left\lbrace PPP; PPF;\dots;FFF \right\rbrace$.

On a $\text{Card}(\Omega)=8$.

On considère le jeu suivant : on mise 2\,\euro{} et on gagne 2\,\euro{} par \og Face \fg{} sorti et on perd 1\,\euro{} par \og Pile \fg{} sorti.

De plus : 3 \og Face \fg{} rapportent 12\,\euro{} et 3 \og Pile \fg{} font perdre 6\,\euro.

À chaque éventualité (ou issue), on associe le gain (positif ou négatif) réalisé :

\begin{itemize}[]
	\item $PPP \rightarrow -8$
	\item $PPF \rightarrow -2$
	\item \dots
	\item $FFF \rightarrow 10$
\end{itemize}
On définit ainsi une fonction de $\Omega$ dans $\R$, notée X et appelée \textbf{variable aléatoire} sur $\Omega$.
\end{cexemple}

\begin{cdefi}
Soit une expérience aléatoire d’univers $\Omega$. On appelle variable aléatoire sur $\Omega$ toute fonction de $\Omega$ dans $\R$.
 
Une variable aléatoire est dite \textbf{discrète} lorsqu'elle prend des valeurs \og isolées \fg.

Une variable aléatoire est notée : X, Y, Z, T, \dots
\end{cdefi}

\subsection{Loi de probabilité}

\begin{cdefi}
Soit X une variable aléatoire définie sur $\Omega$ et soit $x$ un nombre réel. La notation $(X=x)$  désigne l’événement qui réunit toutes les éventualités associées au nombre $x$.

La probabilité de cet événement est notée : $P(X=x)$.
\end{cdefi}

\begin{cexemple}[Compteur=false]
Avec les données précédentes, $P(X=-2)=\dfrac38$, $P(X=10)=\dfrac18$ et $P(X=5)=0$
\end{cexemple}

\begin{cdefi}
La \textbf{loi de probabilité} d’une variable aléatoire X est la fonction qui, à chaque valeur $x$ prise par X, associe la probabilité de l’événement $(X=x)$. On résume cette loi dans un tableau :

\begin{center}
	\begin{tblr}{width=10cm,hlines,vlines,colspec={Q[l,m]*{4}{X[m,c]}}}
		Valeurs $x_i$ prises par X & $x_1$ & $x_2$ & \dots & $x_n$ \\
		Probas $p_i=P(X=x_i)$ & $p_1$ & $p_2$ & \dots & $p_n$ \\
	\end{tblr}
\end{center}
\end{cdefi}

\begin{cexemple}[Compteur=false]
Avec les données précédentes :

\begin{center}
	\begin{tblr}{width=10cm,hlines,vlines,colspec={Q[l,m]*{5}{X[m,c]}}}
		Valeurs $x_i$ & $-8$ & $-2$ & $+1$ & $10$ & Total \\
		Probas $p_i$ & $\nicefrac{1}{8}$ & $\nicefrac{3}{8}$ & $\nicefrac{3}{8}$ & $\nicefrac{1}{8}$ & $1$ \\
	\end{tblr}
\end{center}
\end{cexemple}

\subsection{Espérance mathématique}

\begin{cdefi}[ModifLabel={ \& Propriété}]
L’espérance mathématique d’une variable aléatoire X est le réel $\Esper{X}=\sum_{i=1}^n x_i \times p_i$.

Ce réel représente la \textbf{valeur moyenne} que peut espérer prendre la variable aléatoire X lorsque l’on répète l’expérience aléatoire un très grand nombre de fois.

Lorsque $\Esper{X}=0$, on dit que le jeu est équitable.
\end{cdefi}

\begin{cexemple}[Compteur=false]
Avec les données précédentes, on a $\Esper{X}=-0,125$ donc le jeu est défavorable au joueur.
\end{cexemple}

\subsection{Variance et écart-type}

\begin{cdefi}[Pluriel]
La variance de la variable aléatoire X est le réel $\Varianc{X}=\Esper{X^2}-[ \Esper{X} ]^2$.

L’écart type de la variable aléatoire X est le réel $\EcType{X}=\sqrt{\Varianc{X}}$.
\end{cdefi}

\begin{cexemple}[Compteur=false]
Avec les données précédentes, $\Varianc{X}=\dfrac{1431}{64} \approx 22,36$ et $\EcType{X} \approx 4,73$.
\end{cexemple}

\section{La loi binomiale}

\subsection{Exemple}

\begin{cintro}[Compteur=false]
On lance un dé (non truqué) à 6 faces numérotées de 1 à 6 et on note le numéro de la face qui apparaît.

On appelle \og \textbf{succès} \fg{} l'événement : \og obtenir la face 4 \fg{} et  \og \textbf{échec} \fg{} l'événement contraire.

L'expérience aléatoire réalisée ici est le lancer d'un dé. Cette expérience a deux issues possibles: le succès ou l'échec. Une telle expérience est une \og \textbf{épreuve de Bernoulli} \fg.

La probabilité du succès est de $\nicefrac{1}{6}$ et celle de l'échec $1-\nicefrac{1}{6}=\nicefrac{5}{6}$.

\medskip

On lance 5 fois de suite le dé et on s'intéresse au nombre d'apparitions de la face 4 au cours de ces 5 lancers.

Les 5 lancers sont \textbf{indépendants} les uns des autres (la probabilité du succès et celle de l'échec restent constantes) et sont répétés dans des conditions \textbf{identiques}. Ils constituent un \textbf{schéma de Bernoulli}.

On définit la variable aléatoire X qui, à chaque série de 5 lancers, associe le nombre de succès obtenus.
Les valeurs prises par X sont : 0, 1, 2, 3, 4, 5 ; et :

\begin{itemize}[leftmargin=*]
	\item $P(X=0)=\left(\dfrac56\right)^5=\dbinom{5}{0} \times \left(\dfrac16 \right)^0 \times \left(\dfrac56\right)^5 \approx \num{0,40188}$  (puisque les lancers sont indépendants).
	\item $P(X=1)=\dbinom{5}{1} \times \left(\dfrac16 \right)^1 \times \left(\dfrac56\right)^4 \approx \num{0,40188}$
	\item $P(X=2)=\dbinom{5}{2} \times \left(\dfrac16 \right)^2 \times \left(\dfrac56\right)^3 \approx \num{0,16075}$
	\item $P(X=3)=\dbinom{5}{3} \times \left(\dfrac16 \right)^3 \times \left(\dfrac56\right)^2 \approx \num{0,03215}$
	\item $P(X=4)=\dbinom{5}{4} \times \left(\dfrac16 \right)^4 \times \left(\dfrac56\right)^1 \approx \num{0,00321}$
	\item $P(X=5)=\dbinom{5}{5} \times \left(\dfrac16 \right)^5 \times \left(\dfrac56\right)^0 \approx \num{0,00013}$
\end{itemize}
\end{cintro}

\begin{crmq}
D'où la loi de probabilité de X :

\begin{center}
	\begin{tblr}{hlines,vlines,width=13cm,colspec={*{8}{X[m,c]}}}
		$x_i$ & 0 & 1 & 2 & 3 & 4 & 5 & Total \\
		$p_i$ & \num{0.40188} & \num{0,40188} & \num{0,16075} & \num{0,03215} & \num{0,00321} & \num{0,00013} & 1 \\
	\end{tblr}
\end{center}

Et si $k$ est un entier compris entre 0 et 5, la probabilité d'avoir $k$ succès au cours des 5 tirages est $$P(X=k)=\binom{5}{k} \times \left(\dfrac16 \right)^k \times \left(\dfrac56\right)^{5-k}.$$
\end{crmq}

\begin{cnota}
La loi de probabilité suivie par la variable aléatoire $X$ est une \textbf{loi binomiale} car elle fait intervenir les coefficients binomiaux de Newton.

\smallskip

On note $X \hookrightarrow \LoiBinomiale{5}{\dfrac16}$.
\end{cnota}

\begin{chistoire}[Compteur=false]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{bernoulli}{}\textbf{Jacob Bernoulli} (1654/1705, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{CH}\!) est l'ainé de la famille des \textit{Bernoulli}, famille de mathématiciens suisses d'origine belge. Ses parents, qui sont de riches commerçants, souhaitent qu'il étudie la théologie et la philosophie, ce qu'il fait d'ailleurs puisqu'il obtint un diplôme dans ces deux disciplines, mais ses intérêts se portent rapidement vers l'astronomie et les mathématiques.

\lettrine[findent=.5em,nindent=0pt,lines=4,image,novskip=0pt]{bernoulli_tombe}{}Il aimait particulièrement la spirale logarithmique, et ses propriétés d'invariance. Il demanda à ce que l'on en grave une sur sa tombe, accompagnée des mots \og {\large\polancienne Eadem mutata resurgo} \fg, qui signifient \og {\large\polancienne Elle renait changée en elle-même} \fg. Hélas, le graveur, mauvais mathématicien, dessina une spirale d'Archimède !
\end{chistoire}

\subsection{Définitions}

\begin{cdefi}[Pluriel]
Une épreuve de Bernoulli est une expérience aléatoire à deux issues nommées généralement \og succès \fg{} et \og échec \fg.

Une variable aléatoire X suit la loi binomiale $\LoiBinomiale{n}{p}$  de paramètres $n$ et $p$ (où $n \in \N$  et $0 \pp p \pp1$ ) lorsque sa loi de probabilité est définie de la manière suivante : $$P(X=k)=\binom{n}{k} \times p^k \times \left(1-p\right)^{n-k}.$$
\end{cdefi}

\subsection{Utilisation de la loi binomiale}

\begin{cprop}
Lorsque l'on répète $n$ fois, dans des conditions identiques et indépendantes, une expérience aléatoire à deux issues (le succès, de probabilité $p$ et l'échec, de probabilité $1-p$) alors la variable aléatoire X qui compte le nombre de succès suit la loi binomiale de paramètres $n$ et $p$.
\end{cprop}

\begin{crmq}
Dans le cas de tirages avec remise, il y a \textbf{indépendance} des tirages puisque la probabilité du succès ne change pas d'un tirage à l'autre. En revanche, dans le cas de tirages sans remise, l'indépendance des tirages n'existe plus puisque la composition de l'urne change d'un tirage à l'autre.  Cependant,  lorsque le nombre d'objets tirés est "petit" par  rapport au nombre total d'objets dans l'urne, on peut considérer que les tirages sont indépendants.
\end{crmq}

\subsection{Paramètres}

\begin{cprop}[Pluriel]
Lorsque la variable aléatoire X suit la loi binomiale $\LoiBinomiale{n}{p}$ alors :

\begin{itemize}[leftmargin=*]
	\item $\Esper{X}=np$
	\item $\Varianc{X}=np(1-p)$
	\item $\EcType{X}=\sqrt{np(1-p)}$
\end{itemize}
\end{cprop}

\section{Loi de Poisson}

\subsection{Exemples}

\begin{cintro}[Compteur=false]
On utilise une \textbf{loi de Poisson} pour modéliser des phénomènes aléatoires où le futur est indépendant du passé et où l'on étudie la réalisation d'un événement pendant des intervalles de même longueur ou dans des espaces de même mesure. L'indépendance des succès est également demandée.
\end{cintro}

\begin{chistoire}[Compteur=false,ComplementTitre={ - Anecdote}]
\lettrine[findent=.5em,nindent=0pt,lines=3,image,novskip=0pt]{poisson}{}\textbf{Siméon Denis Poisson} (1781/1840, \worldflag[width=0.5\baselineskip,framewidth=0.25pt]{FR}\!) est un mathématicien, géomètre et physicien français. \\
Ayant montré des signes de talent précoce comme mathématicien, il fut envoyé à l’École centrale de Fontainebleau, et eut la chance d’avoir un enseignant, M. Billy, qui, quand il s’aperçut rapidement que son élève dépassait son maître, se dévoua pour apprendre les branches plus difficiles afin de le suivre et l'encourager, et prédit sa célébrité à venir en utilisant ces vers de Jean de La Fontaine :

\hfill~\og {\large\polancienne Petit Poisson deviendra grand, Pourvu que Dieu lui prête vie.}\fg\hfill~
\end{chistoire}

\begin{cexemple}[Pluriel,Compteur=false]
Les arrivées des automobiles au péage de M. un lundi entre 17 h et 19 h ont été observées statistiquement :
\begin{itemize}[leftmargin=*]
	\item aucun événement extérieur ne vient perturber le trafic et, en moyenne, un automobiliste se présente toutes les minutes, autrement dit, le nombre moyen d'arrivées ne dépend pas de l'heure, il est simplement proportionnel au temps : en 15 minutes par exemple, on compte en moyenne 15 arrivées ;
	\item dans un intervalle de temps d'une minute, il n'est pas impossible, mais rare, que plus d'un automobiliste se présente ;
	\item les nombres d'arrivées pendant deux intervalles de temps disjoints sont des variables aléatoires indépendantes.
\end{itemize}
Considérons la variable aléatoire  X qui, à chaque tranche de 15 minutes, associe le nombre d'automobilistes qui arrivent au péage. La variable aléatoire X suit la loi de Poisson de paramètre 15.

\medskip

Après une étude statistique, on peut affirmer que la variable aléatoire Y mesurant le nombre de clients qui se présentent au  guichet "Affranchissement" du bureau de poste de la commune de  L. par intervalles de temps d'une durée de 10 minutes, entre 14 h 30  et 16 h 30, suit la loi de Poisson de paramètre 5.
\end{cexemple}

\subsection{Définition et propriétés}

\begin{cdefi}[ModifLabel={ \& Propriété}]
Une variable aléatoire X  suit la loi de Poisson $\LoiPoisson{\lambda}$ de paramètre $\lambda$ positif lorsque sa loi de probabilité est définie par $$\forall k \in \N \text{, } P(X=k)=\e^{-\lambda} \times \dfrac{\lambda^k}{k!}.$$
Cette formule n'est pas à mémoriser ; on utilisera la \textit{calculatrice} pour tout calcul de probabilité.
\end{cdefi}

\begin{cexemple}[Compteur=false]
Avec le premier exemple :

\begin{itemize}
	\item $P(X=20) \approx \num{0,0418}$
	\item $P(X \pp 20) \approx \num{0,9170}$
\end{itemize}
Avec le second exemple : $P(X \pg 8) = 1- P(X \pp 7) \approx 1 - \num{0,0180} \approx \num{0,9820}$
\end{cexemple}

\subsection{Paramètres}

\begin{cprop}[Pluriel]
Si X est une variable aléatoire qui suit la loi de Poisson de paramètre $\lambda$ alors :

\begin{itemize}[leftmargin=*]
	\item $\Esper{X}=\lambda$
	\item $\Varianc{X}=\lambda$
	\item $\EcType{X}=\sqrt{\lambda}$
\end{itemize}
\end{cprop}

\begin{crmq}
Le paramètre $\lambda$ est la moyenne des valeurs de la variable aléatoire X : $\lambda$ est le nombre moyen de fois que l'événement considéré (le succès) a été réalisé pendant  l'intervalle de temps étudié ou bien dans l'espace étudié.
\end{crmq}

\subsection{Approximation d'une loi binomiale par une loi de Poisson}

\begin{cidee}[Compteur=false]
On montre que la loi de Poisson est une approximation de la loi binomiale dans le cas où la probabilité du succès (ou de l'échec) est très faible et le nombre de tirages important.

Si $n$ est \og très grand \fg, $p$ \og voisin de 0 \fg{} et $np$ \og pas trop grand \fg, alors la loi $\LoiBinomiale{n}{p}$ est très proche de la loi $\LoiPoisson{\lambda}$ où $\lambda=np$. Il y a conservation de l'espérance mathématique.

On convient en général d'utiliser cette approximation lorsque $n \pg 30$,   $p \pp 0,1$ et $np<15$  ou lorsque $n \pg 50$, $p \pp 0,1$ et $np \pp 10$ (Ces résultats ne sont pas à mémoriser).
\end{cidee}

\begin{cexemple}[Compteur=false]
Sous l'hypothèse que 2\,\% des êtres humains sont gauchers, calculer la valeur arrondie à $10^{-3}$ de la probabilité que, parmi 100 personnes, 3 au plus soient gauchères.

\textbf{Loi binomiale :}

Soit  X la variable aléatoire qui, à chaque groupe de 100 personnes associe le nombre de gauchers. X suit la loi $\LoiBinomiale{100}{\num{0.02}}$ et $P(X \pp 3) \approx \num{0,85896}$.

\textbf{Approximation avec une loi de Poisson :}

Sachant qu'il y a conservation de l'espérance, la loi de Poisson qui approche la loi binomiale de paramètres 100 et $0,02$ est la loi de Poisson $\LoiPoisson{\lambda}$ où $\lambda=100 \times 0,02=2$.

Soit Y une variable aléatoire qui suit la loi de Poisson de paramètre 2, alors $P(Y \pp 3) \approx \num{0,85712}$ (les résultats sont très proches).
\end{cexemple}

\end{document}