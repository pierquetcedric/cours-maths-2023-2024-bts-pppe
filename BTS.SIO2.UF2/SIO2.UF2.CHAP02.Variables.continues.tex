% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{CascadiaMono}[Scale=MatchLowercase]
\usepackage[boites]{cp-preambule}
\usepackage{worldflags}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe={BTS SIO2},matiere={[UF2]},typedoc={CHAP},numdoc={2},titre={Variables continues}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
%divers
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faDice,type=CHAP,swap}
\part{Variables aléatoires continues}%<maincolor=TitreMarron,logo=\faDice,type=\DonneesTypeDoc,swap>

\section{Introduction}

\begin{cexemple}[Pluriel]
On tire à l'arc sur un disque de rayon R et la variable aléatoire $X$ mesure la distance de l'impact de la flèche au centre du disque :

\hspace{5mm}$\rightsquigarrow X$ peut prendre toutes les valeurs de l'intervalle $I=[0;R]$.

\smallskip

La variable aléatoire $X$ représente la durée de vie d'une ampoule électrique :

\hspace{5mm}$\rightsquigarrow X$ peut, théoriquement, prendre toutes les valeurs de l'intervalle $I= \IntervalleFO{0}{+\infty}$.
\end{cexemple}

\begin{cidee}[Pluriel]
On aura donc $P(X=x)=0$ pour tout $x \in I$ et on s'intéressera plutôt à des événements du type $X \pp b$ ou \mbox{$X > a$} ou \mbox{$a < X < b$}. C'est alors la fonction $F$ telle que $F(x)=P(X \pp x)$ qui va jouer un rôle dans le \textbf{calcul} des probabilités.
\end{cidee}

\section{Définitions}

\subsection{Fonction de répartition}

\begin{cdefi}
Soit $X$ une variable aléatoire continue définie sur l'intervalle $I=\IntervalleFF{a}{b}$, on note $F$ la fonction (dérivable) de répartition de $X$ définie par $F(x)=P(X \pp x) \quad \forall x \in I$.
\end{cdefi}

\subsection{Densité de probabilités}

\begin{cdefi}
Soit $X$ une v.a. continue sur l'intervalle $I=\IntervalleFF{a}{b}$, de fonction de répartition $F$.

La fonction $f$, continue et positive, définie par $F'=f$ vérifie \[P(a \pp X \pp b)=\int_a^b f(x) \dx \] est la fonction de densité de la variable aléatoire $X$.
\end{cdefi}

\begin{cillustr}[Compteur=false]
Graphiquement, cette probabilité représente l'aire sous la courbe $f$ entre les verticales $x=a$ et $x=b$.
\end{cillustr}

\begin{cprop}[Pluriel]
On a les résultats suivants :
\begin{itemize}
	\item $P(x=k)=0$ donc $P(X \pp k) = P(X < k)$ (pas de différence entre inégalités larges et strictes) ;
	\item $P(a \pp X \pp b)=1=\displaystyle\int_a^b f(x) \dx$.
\end{itemize}
\end{cprop}

\subsection{Paramètres d'une loi continue}

\begin{cprop}[Pluriel]
Si $X$ est une variable aléatoire continue sur I, de densité de probabilités la fonction $f$, alors on a \[\Esper{X}=\int_I tf(t) \dx[t] \qquad \Varianc{X}=\int_I (t-\Esper{X})^2 f(t) \dx[t] = \int_I t^2 f(t) \dx[t] - (\Esper{X})^2 \qquad \sigma(X) = \sqrt{\Varianc{X}}.\]
\end{cprop}

\pagebreak

\section{La loi uniforme}

\subsection{Définition}

\begin{cdefi}
la variable aléatoire continue $X$ suit la loi de probabilité uniforme sur $[a;b]$, avec $a<b$,  si sa densité de probabilité est constante sur $[a;b]$, c'est-à-dire : \[f \: : \: x \mapsto \dfrac{1}{b-a}.\]
On note $X \sim \mathscr{U}_{[a;b]}$
\end{cdefi}

\subsection{Propriétés}

\begin{cprop}
Si $X \sim \mathscr{U}_{[a;b]}$, alors \[P(x_1 \pp X \pp x_2) = \dfrac{x_2-x_1}{b-a}=\dfrac{\text{largeur voulue}}{\text{largeur totale}}.\]
\end{cprop}

\begin{cprop}[Pluriel]
De plus, on a les paramètres :\[\Esper{X}=\dfrac{a+b}{2} \text{ et } \Varianc{X}=\dfrac{(b-a)^2}{12}.\]
\end{cprop}

\section{La loi normale}

\subsection{Définitions}

\begin{cintro}[Compteur=false]
Une \textbf{loi normale} intervient dans la modélisation de phénomènes aléatoires possédant de nombreuses causes indépendantes dont les effets s’ajoutent, sans que l’un d’eux soit dominant.

\smallskip

Compte tenu de la complexité des processus industriels ou de laboratoire et des phénomènes économiques et sociaux, la loi normale apparaît dans de nombreux secteurs, y compris pour les calculs d’erreurs.
\end{cintro}

\begin{cdefi}
La loi normale notée $\mathscr{N}(m;\sigma)$ d’espérance (ou de moyenne) $m$ et d’écart type $\sigma$ est la loi continue dont la fonction de densité est définie sur $\R$ par : \[f(t)=\dfrac{1}{\sigma \sqrt{2\pi}}\e^{-\tfrac12 \left( \tfrac{t-m}{\sigma}\right)^2}.\]
\end{cdefi}

\begin{cillustr}
La courbe de la fonction $f$ est appelée courbe en cloche.

L'aire $F(x)$ de la partie de plan limitée par l’axe des abscisses, la courbe, la droite d’équation $t=x$ et illimitée à gauche est notée : \[\int_{-\infty}^x f(t) \dx[t].\]

$F$ est la \textbf{fonction de répartition} de la loi normale centrée réduite.
\end{cillustr}

\begin{crmq}[Pluriel]
Dans le cas général où $m$ et $\sigma$ sont des réels quelconques, la droite d’équation $x=m$ est axe de symétrie de la représentation graphique de la fonction de densité $f$ et l’aire totale comprise sous la courbe est toujours égale à 1.

\smallskip

Plus l'écart-type est petit, plus la courbe en cloche est resserrée autour de son axe de symétrie, et inversement.

\smallskip

Les observations graphiques permettent d’expliquer qu’une loi normale peut concerner une variable aléatoire $X$ prenant ses valeurs dans une partie seulement de $\R$.
\end{crmq}

\subsection{Calculs de probabilités}

\begin{crappel}[Compteur=false]
Une probabilité avec la loi normale est l’aire d’une partie du plan située sous la courbe de la fonction de densité.

Pour obtenir les valeurs numériques des probabilités cherchées, on utilise la calculatrice ou un tableur.

\hspace{5mm}$\bullet~~$\ccalc{\texttt{normCD}} ;

\hspace{5mm}$\bullet~~$\ccaln{\texttt{normalcdf}}.
\end{crappel}

\begin{cprop}[s]
Si $X \sim \mathscr{N} (m;\sigma)$, alors on a :
\begin{itemize}
	\item $P(a \pp X \pp b)=P(X \pp b)-P(X \pp a)=F(b)-F(a)$ ;
	\item $P(X \pg a)=1-P(X \pp a)=1-F(a)$ ;
	\item $P(X \pp m)=P(X \pg m)=0,5$ (symétrie).
\end{itemize}
\end{cprop}

\begin{cprop}[Pluriel]
On admet que :
\begin{itemize}
	\item $P( m - \phantom{3}\sigma \pp X \pp m + \phantom{3}\sigma) \approx 0,68$ ;
	\item $P( m - 2\sigma \pp X \pp m + 2\sigma) \approx 0,95$ ;
	\item $P( m - 3\sigma \pp X \pp m + 3\sigma) \approx 0,997$.
\end{itemize}
\end{cprop}

\subsection{Approximation d'une loi binomiale par une normale}

\begin{cprop}
On admet que : 

\smallskip

Si $n$ est grand et $p$ n’est \og ni trop voisin de 0 ni trop voisin de 1 \fg{} alors la loi binomiale $\mathscr{B}(n;p)$ peut être approchée par la loi normale $\mathscr{N} (m;\sigma)$ de même espérance et de même écart type. \[m=np \quad \text{et} \quad \sigma=\sqrt{np(1-p)}.\]
En pratique, l’approximation sera considérée comme valable lorsque $n \pg 30$, $np \pg 5$ et $n(1-p) \pg 5$.
\end{cprop}

\begin{crmq}
Pour \og compenser \fg{} les erreurs dues à l'approximation, on doit procéder à une correction de continuité :

\smallskip

\hspace{5mm}$\bullet~~X \sim \mathscr{B}(n;p)$ est approchée par $Y \sim \mathscr{N}(np;\sqrt{np(1-p)})$ ;

\hspace{5mm}$\bullet~~P(a \pp X \pp b) \sim P( a-0,5 \pp Y \pp b+0,5)$.
\end{crmq}

\end{document}