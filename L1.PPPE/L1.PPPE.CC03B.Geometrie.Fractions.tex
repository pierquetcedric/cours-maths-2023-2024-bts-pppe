% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\usepackage{WriteOnGrid}
\usepackage{postit}
\usepackage{tikz2d-fr}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{CascadiaMono}[Scale=MatchLowercase]
\usepackage[boites]{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U14] Durée : 55 minutes},typedoc=CC,numdoc=03,mois=Jeudi 12 décembre,annee=2024,titre={CC03b}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - \DonneesMois{} \DonneesAnnee}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%divers

\begin{document}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faGraduationCap,type=CC,swap}
\part{Partie B - Géométrie et fractions}%<maincolorTitreMarron,logo={\faGraduationCap},type=\DonneesTypeDoc,swap>

\smallskip

\nomprenomtcbox

\hfill\MiniPostIt*{\small$\leftrightsquigarrow$ \textsf{Le sujet est à rendre avec la copie. Les deux parties du CC sont à faire sur des copies séparées.} $\leftrightsquigarrow$}\hfill~

\smallskip

\begin{crappel}[Compteur=false]
On rappelle que la formule donnant l'aire d'un triangle est $\mathscr{A}=\frac{b \times h}{2}$.
\end{crappel}

\smallskip

\begin{EnvtExo}[Decoration=Points/3,CodeDebut=\medskip]%exo1 := géométrie de base
Construire, \uline{uniquement à l'aide d'un compas et d'une règle non graduée}, et en laissant les traits de constructions, le cercle circonscrit du triangle suivant.

\smallskip

\textit{Les étapes de la construction sont à détailler sur la copie.}

\vspace*{7.5mm}

\begin{center}
	\begin{tikzpicture}[scale=0.75,baseline=(current bounding box.center),line join=bevel]
		\draw[thick] (0,0)--(-5.5,1)--(-1.5,4.25)--cycle;
	\end{tikzpicture}
\end{center}
\end{EnvtExo}

\vspace*{15mm}

\medskip

\begin{EnvtExo}[Decoration=Points/9,CodeDebut=\medskip]%exo2 := fraction(s)
\begin{enumerate}
	\item Simplifier, en \underline{détaillant}, les fractions suivantes sous forme irréductible :
	\begin{enumerate}[itemsep=4pt]
		\item $\dfrac{35}{25}$ ;
		\item $\dfrac{\num{2310}}{78}$ ;
		\item $\dfrac{756}{450}$.
	\end{enumerate}
	\item Pour chacun des nombres suivants, préciser s'il est décimal ou non, et \underline{justifier} :
	\begin{enumerate}[itemsep=4pt]
		\item $\dfrac{65}{8}$ ;
		\item $\dfrac{9}{17}$ ;
		\item $\dfrac{762}{15}$.
	\end{enumerate}
	\item \underline{Recopier} et compléter les pointillés pour que les égalités soient vraies :
	\begin{enumerate}[itemsep=4pt]
		\item $\dfrac{5}{7}=\dfrac{\ldots}{42}=\dfrac{100}{\ldots}$ ;
		\item $\dfrac{64}{100}=\dfrac{\ldots}{25}=\dfrac{32}{\ldots}$.
	\end{enumerate}
	\item \underline{Justifier}, sans \textit{passer par les valeurs décimales}, que les fractions $\dfrac{32}{14}$ et $\dfrac{80}{35}$ sont égales.
\end{enumerate}
\end{EnvtExo}

\pagebreak

\begin{EnvtExo}[Decoration=Points/8,CodeDebut=\medskip]%exo3 géométrie avancée
\textit{Les trois questions suivantes sont indépendantes.}

\begin{enumerate}
	\item $ABC$ est un triangle rectangle en $A$ tel que $BC = 41$~cm et $AB=9$~cm. \underline{Calculer} la longueur $AC$.
	\item On donne la figure à main levée suivante (on rappelle que les proportions/angles ne sont pas \textbf{forcément} respectés !) :
	
	\smallskip
	
	\hfill\begin{tikzpicture}[every node/.style={inner sep=2pt,outer sep=1pt}]
		\tkzDefPoints{0/0/C,2/0/D,6/0.05/E,0/1.85/A,5.9/3.5/B}
		\tkzMarkRightAngles[thick](B,E,D A,C,D)
		\tkzMarkSegments[thick,mark=s||](A,B B,D)
		\tkzMarkSegments[thick,mark=s|](C,D C,A)
		\tkzDrawPolygons[thick,mainlevee](A,D,C D,E,B)
		\tkzDrawSegment[thick,mainlevee](A,B)
		\tkzMarkAngle[thick,size=0.75,mark=|](D,A,B)
		\tkzMarkAngle[thick,size=0.75,mark=||](D,B,E)
		\tkzLabelAngle[pos=1.15,font=\footnotesize](D,A,B){$\ang{50}$}
		\tkzLabelAngle[pos=1.15,font=\footnotesize](D,B,E){$\ang{15}$}
		\tkzLabelPoints[below left](C)
		\tkzLabelPoints[below right](E)
		\tkzLabelPoints[below](D)
		\tkzLabelPoints[above left](A)
		\tkzLabelPoints[above right](B)
	\end{tikzpicture}\hfill~

	\smallskip
	
	Les points $C$, $D$ et $E$ sont-ils alignés ? (\underline{Détailler} les calculs et le raisonnement)
	\item On considère la figure ci-dessous (à main levée et pas en vraie grandeur), dans lequel le triangle $ARC$ est rectangle en $R$. Les unités sont données en cm.
	
	\smallskip
	
	\hfill\begin{tikzpicture}[scale=0.85,every node/.style={inner sep=2pt,outer sep=1pt}]
		\tkzDefPoints{0/0/A,5/0/R,5/3.5/C,2.74/4.76/L}
		\tkzMarkRightAngle[thick](A,R,C)
		\tkzDrawPolygon[thick,mainlevee](A,R,C)
		\tkzDrawSegments[thick,mainlevee](A,L L,C)
		\tkzLabelPoints[below left](A)
		\tkzLabelPoints[below right](R)
		\tkzLabelPoints[above](L)
		\tkzLabelPoints[right](C)
		\tkzLabelSegment[below,sloped](A,R){$10$~cm}
		\tkzLabelSegment[above,sloped](C,R){$7$~cm}
		\tkzLabelSegment[above,sloped](L,C){$5$~cm}
		\tkzLabelSegment[above,sloped](A,L){$11$~cm}
	\end{tikzpicture}\hfill~
	\begin{enumerate}
		\item Calculer, en justifiant, la longueur $AC$.
		\item Calculer l'aire du triangle $ARC$.
		\item Le triangle $LAC$ est-il rectangle ? Justifier rigoureusement la réponse.
	\end{enumerate}
\end{enumerate}
\end{EnvtExo}

\medskip

\begin{EnvtExo}[Type=Perso/Exercice bonus,Decoration=Points/1,CodeDebut=\medskip,Compteur=false]%exobonus
Déterminer, en justifiant, l'écriture fractionnaire du nombre $13,5\overline{72}$.
\end{EnvtExo}

\bigskip

\AffQuadrillage[Grille=Seyes,NbCarreaux=Auto,Marge=2]<\CoulSeyes>

\end{document}
