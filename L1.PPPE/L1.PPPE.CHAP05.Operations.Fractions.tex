% !TeX TXS-program:compile = txs:///arara
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode}
% arara: lualatex: {shell: no, synctex: no, interaction: batchmode} if found('log', '(undefined references|Please rerun|Rerun to get)')

\documentclass[french,a4paper,11pt]{article}
\usepackage{ProfLycee}
\useproflyclib{ecritures}
%\useproflyclib{piton}
%\usepackage[executable=python,ignoreerrors]{pyluatex}
\usepackage[math-style=french,bold-style=ISO]{fourier-otf}
\setsansfont{Fira Sans}[Scale=MatchLowercase]
\setmonofont{CascadiaMono}[Scale=MatchLowercase]
\usepackage[boites]{cp-preambule}
\usepackage{babel}
\graphicspath{{./graphics/}}
%variables
\donnees[classe=L1 PPPE,matiere={[U24]},typedoc=CHAP,numdoc=05,titre={Opérations fractions}]

%formatage
\hypersetup{pdfauthor={Pierquet},pdftitle={\currfilebase},allbordercolors=white,pdfborder=0 0 0,pdfstartview=FitH}
\lhead{\entete{\DonneesMatiere}}
\chead{\entete{\lycee}}
\rhead{\entete{\DonneesClasse{} - Chapitre \thepart}}
\lfoot{\pied{\LicenceDocEtalabV}}
\cfoot{\logolycee{}}
\rfoot{\pied{\NumeroPage}}
%---divers---

\begin{document}

\pagestyle{fancy}

\setKVdefault[tkzBannerpartKeys]{maincolor=TitreMarron,logo=\faDivide,type=CHAP,swap}
\part{Compléments sur les fractions, ensembles de nombres}%<maincolorTitreMarron,logo=\faDivide,type=\DonneesTypeDoc,swap>

\section{Nombres rationnels}

\subsection{Définition, rappels}

\begin{cdefi}
On dit qu’un nombre $x$ est rationnel s’il peut s’écrire comme une fraction, c’est-à-dire s’il existe un entier relatif $a$ et un entier naturel non nul $b$ tel que $x = \dfrac{a}{b}$.

On dit alors que $\dfrac{a}{b}$ est une écriture fractionnaire de $x$.
\end{cdefi}

\begin{cnota}
L’ensemble des nombres rationnels se note $\Q$.
\end{cnota}

\begin{chistoire}[Compteur=false]
« Numérateur » vient du mot latin \textit{numerator}, qui signifie « celui qui compte ».

« Dénominateur » est emprunté du latin \textit{denominator}, qui signifie « celui qui désigne », c'est donc celui qui décide, celui qui indique « par combien on partage ».

Une fraction peut être donc être interprétée en « combien de morceaux on va prendre lors d'un partage ».
\end{chistoire}

\subsection{Premières propriétés}

\begin{cprop}[Pluriel]
\begin{itemize}[leftmargin=*]
	\item Un nombre $x$ est rationnel si et seulement s’il existe un entier $b$ tel que $b \times x$ est un entier.
	\item Tout nombre entier relatif est un nombre rationnel puisque si $x$ est un entier alors $x =
	\dfrac{x}{1}$ avec $x \in Z$ et $1 \in \N$ (on dit que $\Z \subset \Q$).
	\item Un rationnel $x$ admet une infinité d’écritures fractionnaires. Parmi toutes les écritures fractionnaires, on privilégie en général l’écriture sous forme d’une fraction \textit{irréductible}.
\end{itemize}
\end{cprop}

\begin{crmq}[Pluriel]
Il est important de savoir rendre une fraction irréductible, dans l'optique de pouvoir faire des calculs de manière plus \textit{simple}.

\smallskip

Les calculatrices permettent de simplifier automatiquement les fractions, mais il est important de savoir le faire \textit{à la main}, par décompositions ou divisions successives.
\end{crmq}

\begin{cthm}
Un nombre décimal est un rationnel (une fraction de deux entiers) dont le dénominateur est 
le produit de puissances de 2 et/ou de 5.
\end{cthm}

\begin{crmq}
Il est donc important de commencer à simplifier la fraction sous forme irréductible pour savoir \textit{facilement} si elle correspond à un nombre décimal.
\end{crmq}

\pagebreak

\section{Opérations sur les fractions}

\subsection{Addition et soustraction}

\begin{cprop}
Si $\dfrac{a}{b}$ et $\dfrac{c}{b}$ sont deux fractions ayant le \underline{même dénominateur} alors :%
\[ \frac{a}{b}+\frac{c}{b}=\frac{a+c}{b} \text{ et } \frac{a}{b}-\frac{c}{b}=\frac{a-c}{b}.  \]
\end{cprop}

\begin{cmethode}
Si les fractions $\dfrac{a}{b}$ et $\dfrac{c}{d}$ à additionner (ou soustraire) n'ont pas le même dénominateur, il faut commencer par les \textbf{mettre} au même dénominateur.

Pour cela, on cherche un multiple commun de $b$ et $d$ (aussi petit que possible !).
\end{cmethode}

\subsection{Multiplication}

\begin{cprop}
Pour multiplier deux fractions $\dfrac{a}{b}$ et $\dfrac{c}{d}$, il suffit de multiplier les numérateurs entre eux et les dénominateurs entre eux :%
\[ \frac{a}{b}\times\frac{c}{d}=\frac{a\times c}{b \times d}. \]

\end{cprop}

\begin{crmq}[Pluriel]
En particulier, il n’y pas besoin que les fractions aient le même dénominateur.

En revanche, on a intérêt à simplifier les fractions avant de faire les produits.
\end{crmq}

\subsection{Division}

\begin{cprop}
Pour diviser une fraction $\dfrac{a}{b}$ par une fraction \underline{non nulle} $\dfrac{c}{d}$, on multiplie $\dfrac{a}{b}$ par l’inverse de $\dfrac{c}{d}$, c’est-à-dire par $\dfrac{d}{c}$ :%
\[ \frac{\frac{a}{b}}{\frac{c}{d}} = \frac{a}{b} \times \frac{d}{c} = \frac{a \times d}{b \times c}. \]
\end{cprop}

\begin{crmq}[Pluriel]
Comme pour la multiplication, il n’est pas nécessaire que les fractions aient le même dénominateur et on a intérêt à simplifier avant de faire les multiplications.
\end{crmq}

\subsection{Puissances}

\begin{cprop}
Soit $n$ un entier naturel.

Pour élever une fraction à la puissance $n$, on élève le numérateur \underline{et} le dénominateur à la puissance $n$ :%
\[ {\left(\frac{a}{b}\right)}^n = \frac{a^n}{b^n}.  \]
\end{cprop}

\pagebreak

\section{Applications}

\subsection{Résolution d'équations du 1\up{er} degré}

\begin{cmethode}
Les opérations algébriques sur les fractions permettent de résoudre des équations du 1\up{er} degré, à coefficients fractionnaires, de la forme :%
\[ Ax+B=Cx+D \text{ avec }A,~B,~C \text{ et } D \text{ des fractions, et } A\neq C. \]
\end{cmethode}

\begin{crmq}
Les techniques de résolution donnent $(A-C)x=D-B \ssi x=\dfrac{D-B}{A-C}$.

\smallskip

Cette formule n'est pas à retenir, car il vaut mieux effectuer les calculs et simplifications \textit{petit à petit}.
\end{crmq}

\subsection{L'ensemble des réels}

\begin{cdefi}
L’ensemble de tous les nombres que nous utilisons est appelé l’ensemble des nombres réels.
\end{cdefi}

\begin{cnota}
L’ensemble des nombres réels se note $\R$.
\end{cnota}

\begin{cprop}
Le nombre $\sqrt{2}$ n’est pas rationnel.
\end{cprop}

\begin{crmq}
Il existe beaucoup d’autres nombres réels qui ne sont pas rationnels comme $\sqrt{3}$, $\sqrt{5}$, $\sqrt{2}-1$, $\pi$, \ldots

Les nombres réels qui ne sont pas rationnels sont appelés les nombres \textit{irrationnels}.
\end{crmq}

\subsection{La droite des réels}

\begin{cprop}[ComplementTitre={ - Représentation graphique}]
Graphiquement, on peut représenter l’ensemble des nombres réels par une droite graduée appelée « droite réelle » (ou « axe réel »).
%
\begin{center}
	\begin{tikzpicture}
		%droite et graduations
		\draw[thick,->,>=latex] (-8.1,0)--(8,0) ;
		\foreach \x in {-8,-7,...,7} {\draw[thick] (\x,3pt)--(\x,-3pt) ;}
		\draw (0,0) node[above=6pt] {0} node[below=6pt] {O} ;
		\draw (1,0) node[above=6pt] {1} node[below=6pt] {I} ;
		\draw[thick,red] (4.75,3pt)--(4.75,-3pt) ;
		\draw[thick,blue] (-5.5,3pt)--(-5.5,-3pt) ;
		\draw[red] (4.75,0) node[above=6pt] {$x$} node[below=6pt] {M} ;
		\draw[blue] (-5.5,0) node[above=6pt] {$x$} node[below=6pt] {M} ;
		\draw[thick,red,<->,>=latex] (0,1)--(4.75,1) node[midway,above] {$x=OM$ si $M \in [OI)$} ;
		\draw[thick,blue,<->,>=latex] (0,1)--(-5.5,1) node[midway,above] {$x=-OM$ si $M \notin [OI)$} ;
	\end{tikzpicture}
\end{center}
%
À chaque point $M$ de cette droite, on peut associer un et un seul nombre réel qui correspond à la distance $OM$ si $M$ appartient à la demi-droite $[OI)$ et à l’opposé de la distance $OM$ sinon.
\end{cprop}

\begin{cdefi}
Dans la correspondance décrite ci-dessus, on dit que le nombre $x$ est l’abscisse du point $M$ sur la droite réelle.

On le note alors $x_M$, ce qui se lit « x indice M ».
\end{cdefi}

\pagebreak

\subsection{Liens entre les ensembles de nombres}

\begin{cprop}
On a les inclusions suivantes : $\N \subset \Z \subset \D \subset \Q \subset \R$.
\end{cprop}

\begin{cillustr}
\begin{center}
	\begin{tikzpicture}
%	\draw[thin,lightgray,xstep=0.5,ystep=0.5] (-7,-5) grid (8,5) ;
%	\filldraw (0,0) circle[radius=2pt];
	%ensembles et labels
	\node[red,fill=red!5!white,very thick,draw,ellipse,minimum width=13.25cm,minimum height=9cm] (EnsR) at (0.5,0) {};
	\node[blue,fill=blue!5!white,very thick,draw,ellipse,minimum width=9.5cm,minimum height=7cm] (EnsQ) at (-0.2,0) {};
	\node[orange,fill=orange!5!white,very thick,draw,ellipse,minimum width=7cm,minimum height=5.25cm] (EnsD) at (0.1,0) {};
	\node[CouleurVertForet,fill=CouleurVertForet!5!white,very thick,draw,ellipse,minimum width=4.7cm,minimum height=3.35cm] (EnsZ) at (-0.15,0) {};
	\node[purple,fill=purple!5!white,very thick,draw,ellipse,minimum width=3cm,minimum height=1.8cm] (EnsN) at (0,0) {};
	\node[red,font=\Large] (NomR) at (8.5,4.5) {$\R$} ;
	\node[blue,font=\Large] (NomQ) at (8.5,2.5) {$\Q$} ;
	\node[orange,font=\Large] (NomD) at (8.5,0.5) {$\D$} ;
	\node[CouleurVertForet,font=\Large] (NomZ) at (8.5,-1.5) {$\Z$} ;
	\node[purple,font=\Large] (NomN) at (8.5,-3.5) {$\N$} ;
	%flèches
	\foreach \Ens/\Coul in {R/red,Q/blue,D/orange,Z/CouleurVertForet,N/purple} {\draw[very thick,->,>=latex,\Coul] (Nom\Ens)--(Ens\Ens) ;}
	%nombres
	\draw[purple!50!black] (0,0) node {0, 1, 2, 3, \ldots} ; %N
	\draw[CouleurVertForet!50!black] (-0.25,1.15) node {$-1$, $-2$, $-3$, \ldots} ; %Z
	\draw[orange!50!black] (2.75,0.75) node {$\frac15$} ; %D
	\draw[orange!50!black] (-2.9,-0.25) node {$\frac{17}{100}$} ; %D
	\draw[orange!50!black] (0.5,-2) node {$-3,14$} ; %D
	\draw[blue!50!black] (1,3) node {$\frac23$} ; %Q
	\draw[blue!50!black] (-4,1.35) node {$\frac13$} ; %Q
	\draw[blue!50!black] (-2.5,-2.5) node {$\frac16$} ; %Q
	\draw[red!50!black] (3,3.5) node {$\pi$} ; %R
	\draw[red!50!black] (-1,4) node {$\sqrt{3}$} ; %R
	\draw[red!50!black] (-5.5,-0.5) node {$\sqrt{2}$} ; %R
	\draw[red!50!black] (1.5,-4) node {$\sqrt{4-\sqrt{3}}$} ; %R
\end{tikzpicture}
\end{center}
\end{cillustr}

\vfill

\begin{chistoire}[Compteur=false]
Au fil de l'histoire, les mathématiciens ont progressivement pris conscience qu'il existait une infinité de nombres, de natures très variées. Ils se sont aperçus qu'il était possible de « ranger » en grandes familles les nombres ayant des propriétés identiques.

\smallskip

L'ensemble $\N$ vient de l'appellation \textit{naturale} attribuée à Peano (italien, 1858--1932).

\smallskip

L'ensemble $\Z$ vient de l'allemand \textit{zahlen} qui signifie compter.

\smallskip

Peano aurait utilisé la lettre $\Q$, première lettre de \textit{quotiente}, mais selon plusieurs sources, pas pour désigner l'ensemble des rationnels. Cette notation viendrait en fait du groupe BOURBAKI, en 1969.

\smallskip

L'ensemble $\R$, défini par Dedekind (allemand, 1831--1916), pour \textit{real}, recouvre les nombres réels.

\medskip

Il existe d'autres ensembles :

\begin{itemize}
	\item $\C$ pour les nombres complexes ;
	\item $\mathbb{H}$ pour les hypercomplexes ;
	\item $\mathbb{O}$ pour les octonions.
\end{itemize}
\end{chistoire}

\end{document}